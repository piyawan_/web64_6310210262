
import {useState,  useEffect} from "react";
import LuckyResult from "../components/LuckyResult";
import Button from '@mui/material/Button';


function LuckNumber(){

    const [number, setNumber ] = useState("");
    const [luckyResult, setLuckyresult ] = useState(-1);
    const [checkresult, setCheckresult ] = useState("");
    const [checkrandom, setCheckrandom ] = useState("");
    const [randomnumber, setRandomnumber ] = useState(0);

   useEffect(() =>{
            setRandomnumber(Math.round((Math.random()*99)+0)) ;
        });

    function Check (){
        let n=parseInt(number);
        let rn=parseInt(randomnumber);
        
        
        setLuckyresult(n);

        if(n==69 && n==rn){
            setCheckresult("ถูกแล้วจ้าาา");
            setCheckrandom("ตรงจ้าาา");

        }else if(n==69 && n!=rn){
            setCheckresult("ถูกแล้วจ้าาา");
            setCheckrandom("ไม่ตรงจ้าา");
            
        }else if(n!=69 && n==rn){
            setCheckresult("ผิด!!");
            setCheckrandom("ตรงจ้าาา");

        }else if(n!=69 && n!=rn){
            setCheckresult("ผิด!!");
            setCheckrandom("ไม่ตรงจ้าา");
        }

    }

    return(
        <div align="letf">
            <div align="center">
                <b>ให้คุกกี้ทำนายกันนน</b>
                <hr />

            กรุณาทายตัวเลขที่ต้องการ ระหว่าง 0-99: <input type="text"
                     value={number}
                     onChange={ (e) => {setNumber(e.target.value);} } /> 
                <br />

                <Button variant="contained" onClick={ () =>{ Check() } }> ทาย </Button>

                { ( luckyResult >=0) &&
                    <div>
                        <hr />
                        ผลการทำนายยยย
                        <LuckyResult
                            result = {checkresult}
                            resultrandom = {checkrandom}
                            num = {randomnumber}

                        />
                    </div>
                }
            </div>
        </div>

        

    );
}


export default LuckNumber;