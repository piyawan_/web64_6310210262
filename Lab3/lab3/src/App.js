import logo from './logo.svg';
import './App.css';
import Header from'./components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Detail from './components/Detail';

function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Detail />
      <Detail />
      <Detail />
      <Detail />
      <Detail />
      <Detail />
      <Detail />
      <Detail />
      <Detail />
      <Detail />
      <Detail />

      <Footer />
    </div>
  );
}

export default App;
