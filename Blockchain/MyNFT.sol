// SPDX-License-Identifier: GPL-3.0

// Created by HashLips
// The Nerdy Coder Clones

// 0xd360715Eb479734A8c6a7298453a47Fb15e8C259

pragma solidity ^0.8.12;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract ChinnaLimitedV2 is ERC721URIStorage, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;
    constructor() ERC721("Chinna","ChinnaLimitedV1"){}

    function totalSupply() public view returns(uint256){
        return _tokenIds.current();
    }
   
    function mintItem(address minter,string memory tokenURI)public onlyOwner returns(uint256){
            _tokenIds.increment();
            uint256 newItemId = _tokenIds.current();
            _mint(minter,newItemId);
            _setTokenURI(newItemId,tokenURI);
            return newItemId;
    }
}
