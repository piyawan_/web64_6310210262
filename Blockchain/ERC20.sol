pragma solidity ^0.8.12;
//SPDX-License-Identifier: UNLICENSED

contract ChinToken2  {


    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);    
    
    
    string public name;
    string public symbol;
    uint public decimals; // 18 decimals is the strongly suggested default, avoid changing it
    uint public _totalSupply;
    uint public myToken;
    uint public tax=0;


    mapping(address => uint) balances;
    mapping(address => mapping(address => uint)) allowed;

    /**
     * Constrctor function
     *
     * Initializes contract with initial supply tokens to the creator of the contract
     0xe0a8008Ca74f8a44bfbb98B2E4BD40d8cc72f970
     */
    constructor() {
        name = "Chin Token2";
        symbol = "CH2";
        decimals = 18;
        _totalSupply = 1000000000000000000000;
        

        balances[msg.sender] = _totalSupply;
        emit Transfer(address(0), msg.sender, _totalSupply);
    }
    
    
    function mint() public {
        myToken=safeAdd(balances[msg.sender],1000000);
        _totalSupply = _totalSupply+1000000;
        balances[msg.sender] = myToken;
        emit Transfer(address(0), msg.sender, 1000000);
        
    }
    
    
    
    function safeAdd(uint a, uint b) public pure returns (uint c) {
        c = a + b;
        require(c >= a);
    }
    function safeSub(uint a, uint b) public pure returns (uint c) {
        require(b <= a); c = a - b; } function safeMul(uint a, uint b) public pure returns (uint c) { c = a * b; require(a == 0 || c / a == b); } function safeDiv(uint a, uint b) public pure returns (uint c) { require(b > 0);
        c = a / b;
    }

    function totalSupply() public view returns (uint) {
        return _totalSupply  - balances[address(0)];
    }
    
    function showTax() public view returns (uint) {
        return tax;
    }


    function balanceOf(address tokenOwner) public view returns (uint balance) {
        return balances[tokenOwner];
    }

    function allowance(address tokenOwner, address spender) public view returns (uint remaining) {
        return allowed[tokenOwner][spender];
    }

    function approve(address spender, uint tokens) public returns (bool success) {
        allowed[msg.sender][spender] = tokens;
        emit Approval(msg.sender, spender, tokens);
        return true;
    }

    function transfer(address to, uint tokens) public returns (bool success) {
        
        balances[msg.sender] = safeSub(balances[msg.sender], tokens);
        balances[to] = safeAdd(balances[to], tokens);
        emit Transfer(msg.sender, to, tokens);
        return true;
    }

    function transferFrom(address from, address to, uint tokens) public returns (bool success) {
        balances[from] = safeSub(balances[from], tokens);
        allowed[from][msg.sender] = safeSub(allowed[from][msg.sender], tokens);
        balances[to] = safeAdd(balances[to], tokens);
        emit Transfer(from, to, tokens);
        return true;
    }

}