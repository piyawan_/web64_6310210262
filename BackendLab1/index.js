const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/bmi', (req, res)=>{
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}
    if(!isNaN(weight)&& !isNaN(height)){
    let bmi = weight/(height*height)
     result = {
        "status" : 200,
        "bmi" : bmi
    }
}else{
        result = {
            "status" : 400,
            "message" : "weight or height is not a number"
        }
    }
    
    res.send(JSON.stringify(result))
})

app.get('/triangle', (req, res)=>{
    let height = parseFloat(req.query.height)
    let base = parseFloat(req.query.base)
    var result={}
    if(!isNaN(base)&& !isNaN(height)){
    let area = (1/2)*base*height
    result = {
        "area" : area
    }
    }else{
        result={
            "massage" : "base or height is not a number"
        }
    }

    res.send(JSON.stringify(result))
})

app.post('/score', (req, res)=>{
    let score = parseFloat(req.query.score)
    let grade = req.query.grade
    var result={}

    if(!isNaN(score)){
    if(score>79){
        grade='A'
    }else if(score >74){
        grade='B+'
    }else if(score >69){
        grade='B'
    }else if(score >64){
        grade='C+'
    }else if(score >59){
        grade='C'
    }else if(score >54){
        grade='D+'
    }else if(score >49){
        grade='D'
    }else if(score <50){
        grade='E'
    }

    result={
        "Grade":grade
    }
    }else{
        result={
            "massage":" Score is not a number"
        }
    }

    res.send(JSON.stringify(result))
})

app.get('/hello', (req, res) => {
    res.send('Hello '+req.query.name)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})